\section{The Vector Measure}

\subsection{Related Work}

Context vectors have been used in the field of Information Retrieval for a
very long time. Documents and queries are represented as large
multidimensional vectors containing co-occurrence counts of words. The
vectors describe the contexts of words. Sch\"utze \cite{Schutze98} uses
such vectors in the task of {\em word sense discrimination}. The Vector
measure was inspired by his work.

Word sense discrimination is the task of grouping instances of an ambiguous
word into clusters, such that each cluster corresponds to a particular
sense of the word. The task, however, does not involve actually labeling
the groups with particular senses. In other words, the different usages of
a particular word are grouped together.

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=13cm]{discrimination.eps}
\end{center}
\caption{Instances containing the word {\em bank}}
\label{fig:discrimination}
\end{figure}

For example, figure \ref{fig:discrimination} lists five instances using the
word {\em bank}. After grouping these instances based on the different
usages of {\em bank}, we would have instances (1), (4) and (5) in a single
group and instances (2) and (3) in another. Note that at the end of this
procecss, we are able to say that in each of the instances (1), (4) and
(5), {\em bank} has the same sense, but we do not have a particular label
for that sense. The task of word sense discrimination is, therefore, just a
step short of word sense disambiguation. A word sense disambiguation
algorithm would actually go ahead and assign sense labels to each of the
groups thus formed

To perform the task of word sense discrimination, Sch\"utze represents each
instance of the ambiguous word by a multidimensional real-valued
vector. To achieve this vector-representation of contexts, the
second-order co-occurrence counts of words, with respect to the ambiguous
word, are used. A detailed description of this algorithm follows.

All the vectors formed in this process are mapped into a multidimensional
space known as the ``Word Space''. This space is so named, because of the
fact that the words of a corpus define the dimensions of this space. Each
word corresponds to a dimension. These words may be selected in numerous
ways. For instance, we could simply use all of the words occurring
in a corpus to define this space. Frequency cutoffs and statistical tests
of association on the words of a corpus have been used by Sch\"utze as some
of the other ways of creating this word space. It can be observed that the
word space turns out to be gigantic (of the order of thousands of
dimensions).

Once the word space is defined, Sch\"utze describes three different
types of vectors that can be mapped into this space -- {\em word vectors},
{\em context vectors} and {\em sense vectors}.

The word vector ($\stackrel{\rightarrow}{w}$) for a word $w$ is a vector
mapped in the word space, that captures the contextual information of
$w$. In order to create the word vector $\stackrel{\rightarrow}{w}$, we
consider each word $v_i$ corresponding to dimension $i$ of the
space. Dimension $i$ of $\stackrel{\rightarrow}{w}$ then contains the
number of times $v_i$ occurs in the vicinity of (in a window of context
around) $w$ in a large corpus. $\stackrel{\rightarrow}{w}$, therefore, is a
vector that captures the contextual information of word $w$. Figure
\ref{fig:coin} shows an example of a word vector for {\em coin} in a
6-dimensional\footnote{In practice, the word space is a lot larger than
this} word space. Note that, in accordance with our intuition, words like
{\em dollar} and {\em cent} occur a lot more in the context of coin, than
words like {\em movie} and {\em noon}.

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=13cm]{coin.eps}
\end{center}
\caption{Example of a word vector for {\em coin}}
\label{fig:coin}
\end{figure}

Context vectors are the second type of vectors that get mapped into the
word space. Each context vector corresponds to an instance of the ambiguous
word. The context vector $\stackrel{\rightarrow}{c_a}$, corresponding to
instance $a$, is the resultant vector formed by adding the word vectors for
all words present in instance $a$.

To illustrate this, suppose we have the an instance of the ambiguous word
{\em art} as follows:

\begin{quote}
  {\em The paintings were displayed in the art gallery.}
\end{quote}

The context vector corresponding this instance would be the sum of the word
vectors for the words {\em painting}, {\em display}, {\em art}, {\em
gallery}, {\em were}, {\em in}, and {\em the}. Figure \ref{fig:context}
shows how the context vector may be computed in a 2-dimensional word
space.

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=13cm]{context.eps}
\end{center}
\caption{Creating a context vector from word vectors}
\label{fig:context}
\end{figure}

Context vectors thus present us with a contextual representation of pieces
of text. Intuitively, the orientation of each vector in the word space
determines the topics that word or context is associated with. The nearness
of two such vectors indicates the extent of the contextual overlap. The
approximate ``distance'' between the meanings of two words is measured by
finding the cosine of the angle between their vectors, which determines the
extent of the overlap of the vectors and measures similarity of
concepts. The lengths of the vectors are weighted by log inverse document
frequencies of the words. Log inverse document frequency is a concept from
the field of Information Retrieval that describes how uniformly a word is
distributed over the text documents under consideration. Words such as
``idea'' or ``help'' are approximately uniformly distributed throughout all
the text documents under consideration and give little information about a
specific subject. Words that are localized in a few small areas of the
documents usually discuss and relate to certain specific topics. Thus the
log inverse document frequency gives an approximate magnitude of the
ability of a word to distinguish between different topics.

The task here is to cluster the instances based on the sense of the
ambiguous word. The context vectors, being a reasonable representation of
the instances, are therefore clustered based on their similarity. The
cosine of the angle between two vectors is used as the measure of
similarity. An iterative Expectation-Maximization (EM) algorithm is used to
cluster the context vectors into groups of similar vectors. Since the
context vectors represent the meaning of the ambiguous words in their
contexts, we would expect the context vectors of all the
ambiguous words having the same sense to have approximately the same
direction in the multidimensional space. Sense vectors are used to
represent the general orientation of each cluster in the word space. A
sense vector corresponding to a cluster is the sum of the context vectors
of all instances in that cluster.

Intuitively, the process described by Sch\"{u}tze puts forth a scale that
measures the extent to which two words are related, but not without some
pitfalls. Firstly, to improve the accuracy and the reliability of the
results, the vectors are weighted. This requires us to calculate
the log inverse document frequencies of all the words in the
word space. Secondly, it is observed that the algorithm gives good
results when the vectors of the words in the context of the ambiguous word
have a high degree of ``discriminating potential'', i.e. the ability to
distinguish between different topics. This implies that the same algorithm
may give different results for the same words in different texts.

Some highly related work, using context vectors, has also been done by
Inkpen and Hirst \cite{InkpenH03}. They attempt to disambiguate
near-synonyms in text using various ``indicators''. Near-synonyms are words
whose senses are almost indistinguishable. There is only a fine difference
between their senses.

Their disambiguation algorithm considered a number of ``indicators'' to
determine the correct sense of the word. The suggestions from the
``indicators'' were then weighted by a decision tree to get the final
result. The decision tree was learnt from a test data set.

One of the ``indicators'' used in the process was based on context
vectors. Using an approach similar to that described by Sch\"{u}tze,
context vectors were created for the context of the word and for the
glosses of each sense of the target word. The glosses were considered as a
bag of words and the word vectors for these words were summed to get the
context vectors corresponding to the glosses. The distance between the
vector corresponding to the text and that corresponding to the gloss was
measured (as the cosine of the angle between the vectors). The nearness of
the vectors was used as an indicator to pick the correct sense of the
target word.

Niwa and Nitta \cite{NiwaN94} compare the approaches to measuring
semantic distance of words -- using a large corpus of text, such as the one
described by Sch\"{u}tze \cite{Schutze98}, and using a dictionary. Semantic
measures that use a large corpus of text to measure distances between words
are hindered because of the insufficient data for rare senses of
words. Niwa and Nitta propose a dictionary based approach, that uses a word
network such as that in an ordinary dictionary, where each word is linked
to other words that occur in its definition (description of its
meaning). Therefore, each word is a node in this network. A set of selected
nodes, usually corresponding to words having medium range frequencies (51
to 1050) in the Collins English Dictionary, are selected as
origins. Vectors corresponding to each word are calculated as the shortest
distances of the word to each of the origins. The distance of a word from
an origin is measured by counting the number of links between nodes
required to be traversed to reach the origin. In a simplistic case the
links are assumed to have a fixed weight. But this treats low-frequency
words equivalent to high frequency words. However, if we consider the
relatively low frequency word {\em limb} that occurs in the definition of
the relatively high frequency word {\em hand}, we see that these two words
({\em hand} and {\em limb}) are more strongly related than {\em hand} and
{\em hold} (which may also occur in the definition of hand). Elaborating on
this, if {\em hand, limb} and {\em hold} occur in a sentence we would
instantly associate {\em hand} with {\em limb} but we cannot say if there
is or is not any relation between {\em hand} and {\em hold}. Thus, we need
to give more weight to links that pass through low frequency words. Taking
word frequency into consideration, the following definition of link weight
is used:
\begin{equation}
length(W_{1}, W_{2})\mathop{=}_{def}-log\left (\frac{n^{2}}{N_{1}
\cdot N_{2}}\right ) 
\end{equation}
where $W_{1}$ and $W_{2}$ are the words, $N_{1}$ and $N_{2}$ are the number
of links from the corresponding words and {\em n} is the number of links
between the words.

Niwa and Nitta compare dictionary based vectors with co-occurrence based
vectors, where the vector of a word is the probability that an origin word
occurs in the context (in a given window of words) of the word. These two
representations are evaluated by applying them to real world applications
and quantifying the results. Both measures are first applied to {\em Word
Sense Disambiguation} and then to the {\em Learning of positives or
negatives}, where it is required to determine whether a word has a positive
or negative meaning. It was observed that the co-occurrence based idea
works better for the {\em Word Sense Disambiguation} and the dictionary
based approach gives better results for the {\em Learning of positive or
negative}. From this, the conclusion is that the dictionary based vectors
contain some different semantic information about the words and warrants
further investigation. It is also observed that for the dictionary based
vectors, the network of words is almost independent of the dictionary that
is used, i.e. any dictionary gives us almost the same
network. Consequently, we are presented with yet another novel way to
think about the semantic relatedness of words.

\subsection{Description}

We introduce a measure of semantic relatedness based on context vectors
that is inspired by Sch\"utze's work. In our approach, each concept in
WordNet is represented by a {\em gloss vector}. A gloss vector is
essentially a context vector formed by considering a WordNet gloss as the
context. The semantic relatedness of two concepts then is simply the cosine
of the angle between the corresponding gloss vectors.

In order to create gloss vectors we start by creating a word space, similar
to the one created by Sch\"utze.. The words selected to define this space
should be highly topical, having great potential to discriminate
topics. Sch\"utze used frequency cutoffs and the $\chi^{2}$ test of
association on the words of a large corpus. For our experiments we use the
WordNet glosses as a corpus, and select content words for the word space
from it. We use a list of stop words to eliminate function words. We
experiment with different frequency cutoffs.

The next step in creating gloss vectors is the creation of word vectors
corresponding to all content words in the WordNet glosses. The process,
again, is similar to that described by Sch\"utze. To create a word vector
for word $w$:
\begin{enumerate}
\item Initialize the vector to a zero vector $\stackrel{\rightarrow}{w}$.
\item Find every occurrence of $w$ in a large corpus.
\item For each occurrence, increment those dimensions of
$\stackrel{\rightarrow}{w}$ that correspond to the words from the word
space, present in a window of context around $w$ in the corpus.
\end{enumerate}
The vector $\stackrel{\rightarrow}{w}$, therefore, encodes the
co-occurrence information of $w$. Using this method we create word vectors
for all content words present in the WordNet glosses. Again, the content
words, whose word vectors are created, can be selected using various means
such as frequency cutoffs, stop-lists, etc. For our experiments, we create
word vectors from the ``WordNet gloss corpus'', a corpus composed of all
the glosses present in WordNet. We consider each gloss as the context.

Intuitively, we can imagine the multidimensional ``word vector space'' to
be composed of a large number of ``pockets of space'', each pocket
corresponding to a certain topic or aspect of the real world. Depending on
their direction, the word vectors are weighted by the various pockets based
on the senses of the word (corresponding to the word vector). To illustrate
this point, consider the word {\em bank}, which could mean ``a financial
institution'' or a ``river bank''. The word vector for {\em bank} would be
weighted by the pockets of the space corresponding to finance and by
pockets of the space related to rivers (and nature). This is because the word
{\em bank} would co-occur with words from these two categories, in a large
corpus. Other pockets, related to the human body for instance, would have
no bearing on this word vector at all. Ideally, every word vector would be
weighted by pockets or topics related to the corresponding word (and it
senses).

Having created the word vectors, the gloss vector for a WordNet concept is
created by adding the word vectors of every content word in its gloss. For
example, consider the gloss of {\em lamp} -- {\em an artificial source of
visible illumination}. The gloss vector for {\em lamp} would be formed by
adding the word vectors of {\em artificial}, {\em source}, {\em visible}
and {\em illumination}.

Again, imagining the ``pocket'' view of the multidimensional space, for a
gloss vector we would hope and expect that the particular pocket in the
space would more highly weight that gloss vector as compared to the other
pockets. This would be because, the content words in the gloss would have
at least one topic in common, and all the word vectors corresponding to
these content words would be weighted by one common pocket. The gloss
vector would, therefore, be more highly weighted towards this common topic
and this common topic describes, to some extent, the concept under
consideration.

This formulation of the Vector measure is independent of the dictionary
used and is independent of the corpus used, and hence is quite
flexible. However, it faces some of the problems faced by Lesk's
\cite{Lesk86} gloss overlap algorithm for word sense disambiguation --
short glosses. To overcome this problem Banerjee and Pedersen
\cite{BanerjeeP02} adapted their Extended Gloss Overlap measure to use the
relations in WordNet to augment the short glosses with other related
glosses. We use the relations in WordNet and augment the glosses in a
similar way for the vector measure. To create a gloss vector with
augmented glosses, consider the gloss of a concept $c$. With this gloss we
concatenate the glosses of all concepts related to $c$ by any WordNet
relation. Also, rather than using all the WordNet relations, we can control
the speed and efficacy of the measure, by carefully selecting the relations
to use for the augmented gloss. The gloss vector for $c$ is then created
from the big concatenated gloss. It is also possible to use other
dictionaries or representations of the concept to build gloss vectors
from.

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=13cm]{vectors.eps}
\end{center}
\caption{A 2-dimensional vector space showing {\em word vectors} and a {\em gloss vector}}
\label{fig:vectors}
\end{figure}

The word vectors as well as the gloss vectors usually have a very large
number of dimensions (usually tens of thousands) and it is very difficult
to visualize this space. Figure \ref{fig:vectors} attempts to illustrate
the vectors in two dimensions (i.e. using a vector space of only 2
dimensions). {\em Tennis} and {\em food} are the dimensions of this
2-dimensional space. We see that the word vector for {\em serve} is
approximately halfway between {\em tennis} and {\em food}, since the word
{\em serve} could mean to ``serve the ball'' in the context of tennis or
could mean ``to serve food'' in another context. The word vectors for {\em
eat} and {\em cutlery} are very close to {\em food}, since they do not have
a sense that is related to tennis. The gloss for the word {\em fork} --
``cutlery used to serve and eat food'' -- contains the words {\em cutlery},
{\em serve} and {\em eat} (and {\em food}). The gloss vector for {\em fork}
is formed by adding the word vectors of {\em cutlery}, {\em serve} and {\em
eat} and {\em food}. Thus, {\em fork} has a gloss vector which is heavily
weighted towards {\em food}. {\em Food} is, therefore, topical of and is
related to the concept of {\em fork}. However, this is a small gloss. Using
augmented glosses, we achieve better representations of concepts to build
gloss vectors upon.

Gloss vectors for all concepts in WordNet can be computed in this
manner. The relatedness of two concepts is then determined as the cosine of
the normalized gloss vectors corresponding to the two concepts:
\begin{equation}
  related_{vector}(c_{1}, c_{2})  = 
  \cos(angle(\stackrel{\rightarrow}{v_{1}},\stackrel{\rightarrow}{v_{2}}))
\end{equation}
where $c_{1}$ and $c_{2}$ are the two given concepts,
$\stackrel{\rightarrow}{v_{1}}$ and $\stackrel{\rightarrow}{v_{2}}$ are the
gloss vectors corresponding to the concepts and $angle$ returns the angle
between vectors. Using vector products we can rewrite the above relatedness
formula as:
\begin{equation}
  related_{vector}(c_{1}, c_{2}) =  \frac{\stackrel{\rightarrow}{v_{1}}
    \cdot \stackrel{\rightarrow}{v_{2}}}{|v_{1}||v_{2}|}
\end{equation}

We now have a measure of semantic relatedness based on WordNet glosses,
which is enhanced with information from a large corpus of text. However, it
should be pointed out that this measure is not dependent on WordNet
glosses, and can be employed with any representation of concepts (such as
dictionary definitions), with co-occurrence counts from any corpus.
