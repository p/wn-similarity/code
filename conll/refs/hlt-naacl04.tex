%
% File hlt-naacl2004.tex
%
% Contact: lindek@cs.ualberta.ca

\documentclass[10pt]{article}
\usepackage{hltnaacl04}
\usepackage{times}
\usepackage{latexsym}

\setlength\titlebox{6.5cm}    % Expanding the titlebox

\title{Instructions for HLT-NAACL-2004 Proceedings}

\author{Author 1\\
  Address 1\\
  {\tt email address} \And
  Author 2\\
  Address 2\\
  {\tt email address}}

\date{}

\begin{document}
\maketitle
\begin{abstract}
  This document contains the instructions for preparing a camera-ready
  manuscript for the proceedings of HLT-NAACL-2004. The document itself conforms
  to its own specifications, and is therefore an example of what
  your manuscript should look like.  Authors are asked to conform to
  all the directions reported in this document.
\end{abstract}

\section{Credits}

This document has been adapted from the instructions for the
ACL-2002 proceedings by Dekang Lin and Eugene Charniak.  It was
adapted in turn from the ACL-02 proceedings by Norbert Reithinger,
Giorgio Satta, and Roberto Zamparelli. The ACL-01 instructions was
elaborated from similar documents used for previous editions of the
ACL and EACL annual meetings. Those versions were written by several
people, including John Chen, Henry S. Thompson and Donald
Walker. Additional elements were taken from the formatting
instructions of the Xth {\em International Joint Conference on
Artificial Intelligence}.

\section{Introduction}

The following instructions are directed to authors of full papers accepted
for publication in the HLT-NAACL-2004 proceedings.  All authors are required
to adhere to these specifications. Since the proceedings will appear
in hardcopy and electronic form, authors are required to provide a
camera-ready {\bf and} a Portable Document Format (PDF) version of
their papers. The hardcopy must be produced with a laser printer at
300 dpi resolution or better, printed on US-Letter (8.5in $\times$
11in) paper. The proceedings will be printed on US-Letter paper.
Authors from countries in which access to word-processing systems is
limited should contact the publication chair Miles Osborne at the
address miles@inf.ed.ac.uk as soon as possible.


\section{General Instructions}

Manuscripts must be in two-column format.  Exceptions to the
two-column format include the title, authors' names and complete
addresses, which must be centered at the top of the first page, and
any full-width figures or tables (see the guidelines in
Subsection~\ref{ssec:first}).
{\bf Type single-spaced.}  Use only one side of the page. Start
all pages directly under the top margin.  See the guidelines later
regarding formatting the first page. 

If the paper is produced by a printer, make sure that the quality
of the output is dark enough to photocopy well.  It may be necessary
to have your laser printer adjusted for this purpose.  Papers that are too
faint to reproduce well may not be included.

{\bf Do not print page numbers on the manuscript.}

The maximum length of a manuscript is eight ($8$) pages for full
papers, three (3) pages for short papers/posters, and two (2) pages
for demonstrations.

\section{Format of Electronic Manuscript}
\label{sect:pdf}

For the production of the electronic manuscript you must use Adobe's
Portable Document Format (PDF). This format can be generated from
postscript files: on Unix systems, you can use {\tt ps2pdf} for this
purpose; under Microsoft Windows, Adobe's Distiller can be used.  Note
that some word processing programs generate PDF which may not include
all the necessary fonts (esp. tree diagrams, symbols). When you print
or create the PDF file, there is usually an option in your printer
setup to include none, all or just non-standard fonts.  Please make
sure that you select of the option of including ALL the fonts.  {\em
  Before sending it, test your {\/\em PDF} by printing it from a
  computer different from the one where it was created}. Moreover,
some word processor may generate very large postscript/PDF files,
where each page is rendered as an image. Such images may reproduce
poorly.  In this case, try alternative ways to obtain the postscript
and/or PDF.  One way on some systems is to install a driver for a
postscript printer, send your document to the printer specifying
``Output to a file'', then convert the file to PDF.

For reasons of uniformity, Adobe's {\bf Times Roman} font should be
used. In \LaTeX2e{} this is accomplished by putting

\begin{quote}
\begin{verbatim}
\usepackage{times}
\usepackage{latexsym}
\end{verbatim}
\end{quote}
in the preamble.

Additionally, it is of utmost importance to specify the US {\bf
  US-Letter format} (8.5in $\times$ 11in) when formatting the paper.
When working with {\tt dvips}, for instance, one should specify {\tt
  -t letter}.


\subsection{Layout}
\label{ssec:layout}

Manuscripts should have two columns to a page, in the manner these
instructions are printed. The exact dimensions for a page on US-letter
paper are:

\begin{itemize}
\item Left and right margins: 1in
\item Top margin:1in
\item Bottom margin: 1in
\item Columns width: 3.15in
\item Column height: 9in
\item Gap between columns: 0.2in
\end{itemize}


\subsection{The First Page}
\label{ssec:first}

Center the title, author's name(s) and affiliation(s) across both
columns. Do not use footnotes for affiliations.  Do not include the
paper ID number that was assigned during the submission process. 
Use the two-column format only when you begin the abstract.

{\bf Title}: Place the title centered at the top of the first page, in
a 15-point bold font. Long title should be typed on two lines without
a blank line intervening. Approximately, put the title at 1in from the
top of the page, followed by a blank line, then the author's names(s),
and the affiliation on the following line.  Do not use only initials
for given names (middle initials are allowed). The affiliation should
contain the author's complete address, and if possible an electronic
mail address. Leave about 0.75in between the affiliation and the body
of the first page.

{\bf Abstract}: Type the abstract at the beginning of the first
column.  The width of the abstract text should be smaller than the
width of the columns for the text in the body of the paper by about
0.25in on each side.  Center the word {\bf Abstract} in a 12 point
bold font above the body of the abstract. The abstract should be a
concise summary of the general thesis and conclusions of the paper.
It should be no longer than 200 words.

{\bf Text}: Begin typing the main body of the text immediately after
the abstract, observing the two-column format as shown in 
the present document. Type single spaced. 
{\bf Indent} when starting a new paragraph. 

{\bf Font:} 
For reasons of uniformity,
use Adobe's {\bf Times Roman} fonts, with 10 points for text and 
subsection headings, 12 points for section headings and 15 points for
the title. If Times Roman is unavailable, use {\bf Computer Modern
  Roman} (\LaTeX2e{}'s default; see section \ref{sect:pdf} above).
Note that the latter is about 10\% less dense than Adobe's Times Roman
font.  

\subsection{Sections}

{\bf Headings}: Type and label section and subsection headings in the
style shown on the present document.  Use numbered sections (Arabic
numerals) in order to facilitate cross references. Number subsections
with the section number and the subsection number separated by a dot,
in Arabic numerals. Do not number subsubsections.

{\bf Citations}: Follow the ``Guidelines for Formatting Submissions''
to {\em Computational Linguistics\/} that appears in the first issue of
each volume, if possible.  That is, citations within the text appear
in parentheses as~\cite{Gusfield:97} or, if the author's name appears in
the text itself, as Gusfield~\shortcite{Gusfield:97}. 
Append lowercase letters to the year in cases of ambiguities.  
Treat double authors as in~\cite{Aho:72}, but write as 
in~\cite{Chandra:81} when more than two authors are involved. 
Collapse multiple citations as in~\cite{Gusfield:97,Aho:72}.

\textbf{References}: Gather the full set of references together under
the heading {\bf References}; place the section before any Appendices,
unless they contain references. Arrange the references alphabetically
by first author, rather than by order of occurrence in the text.
Provide as complete a citation as possible, using a consistent format,
such as the one for {\em Computational Linguistics\/} or the one in the 
{\em Publication Manual of the American 
Psychological Association\/}~\cite{APA:83}.  Use of full names for
authors rather than initials is preferred.  A list of abbreviations
for common computer science journals can be found in the ACM 
{\em Computing Reviews\/}~\cite{ACM:83}.

The provided \LaTeX{} and Bib\TeX{} style files roughly fit the
American Psychological Association format, allowing regular citations, 
short citations and multiple citations as described above.

{\bf Appendices}: Appendices, if any, directly follow the text and the
references (but see above).  Letter them in sequence and provide an
informative title: {\bf Appendix A. Title of Appendix}.

\textbf{Acknowledgements} sections should go as a last section immediately
before the references.  Do not number the acknowledgement section.

\subsection{Footnotes}

{\bf Footnotes}: Put footnotes at the bottom of the page. They may
be numbered or referred to by asterisks or other
symbols.\footnote{This is how a footnote should appear.} Footnotes
should be separated from the text by a line.\footnote{Note the
line separating the footnotes from the text.}

\subsection{Graphics}

{\bf Illustrations}: Place figures, tables, and photographs in the
paper near where they are first discussed, rather than at the end, if
possible.  Wide illustrations may run across both columns. Do not use
color illustrations as they may reproduce poorly.

{\bf Captions}: Provide a caption for every illustration; number each one
sequentially in the form:  ``Figure 1. Caption of the Figure.'' ``Table 1.
Caption of the Table.''  Type the captions of the figures and 
tables below the body, using 10 point text.  




%\bibliographystyle{acl}
%\bibliography{sample}

\begin{thebibliography}{}

\bibitem[\protect\citename{Aho and Ullman}1972]{Aho:72}
Alfred~V. Aho and Jeffrey~D. Ullman.
\newblock 1972.
\newblock {\em The Theory of Parsing, Translation and Compiling}, volume~1.
\newblock Prentice-{Hall}, Englewood Cliffs, NJ.

\bibitem[\protect\citename{{American Psychological Association}}1983]{APA:83}
{American Psychological Association}.
\newblock 1983.
\newblock {\em Publications Manual}.
\newblock American Psychological Association, Washington, DC.

\bibitem[\protect\citename{{Association for Computing Machinery}}1983]{ACM:83}
{Association for Computing Machinery}.
\newblock 1983.
\newblock {\em Computing Reviews}, 24(11):503--512.

\bibitem[\protect\citename{Chandra \bgroup et al.\egroup }1981]{Chandra:81}
Ashok~K. Chandra, Dexter~C. Kozen, and Larry~J. Stockmeyer.
\newblock 1981.
\newblock Alternation.
\newblock {\em Journal of the Association for Computing Machinery},
  28(1):114--133.

\bibitem[\protect\citename{Gusfield}1997]{Gusfield:97}
Dan Gusfield.
\newblock 1997.
\newblock {\em Algorithms on Strings, Trees and Sequences}.
\newblock Cambridge University Press, Cambridge, UK.

\end{thebibliography}

\end{document}
