\documentclass[10pt]{article}
\usepackage{hltnaacl04}
\usepackage{times}
\usepackage{latexsym}

\setlength\titlebox{6.5cm}    % Expanding the titlebox

\title{WordNet::Similarity - Measuring the Relatedness of Concepts}

\author{Ted Pedersen\\
  Department of Computer Science \\
  University of Minnesota \\
  Duluth, MN 55812 \\
  {\tt tpederse@d.umn.edu} \And
  Siddharth Patwardhan \\
  School of Computing \\  
  University of Utah \\
  Salt Lake City, UT 84102\\
  {\tt sidd@cs.utah.edu} \AND 
  Jason Michelizzi \\
  Department of Computer Science \\
  University of Minnesota \\
  Duluth, MN 55812 \\
  {\tt mich0212@d.umn.edu} \And
  Satanjeev Banerjee \\
  Language Technologies Institute \\
  Carnegie Mellon University \\
  Pittsburgh, PA 15213 \\
  {\tt satanjeev@cs.cmu.edu}}

\begin{document}
\maketitle

\section{Introduction}

WordNet::Similarity is a freely available software package that makes it  
possible to measure the semantic similarity and relatedness  between a 
pair of word senses (or concepts). It provides ten different measures, all  
of  which are based on the lexical database WordNet. These measures are 
implemented as  Perl modules, and support methods that take as input two 
word senses, and  return a numeric value that represents the degree to 
which they are related. 

Such measures have a wide range of application. For example, in order  
to determine which sense of a given word is used in a  particular  
context, the SenseRelate package ({\em senserelate.sourceforge.net}) uses   
WordNet::Similarity to determine which sense of that target word
is most related to the words that surround it. 

WordNet::Similarity can be utilized in three different ways. It can be 
incorporated into source code by including it as a Perl module, and then  
accessing the methods that it  provides to measure relatedness. There is  
also a command line interface provided by the program {\it similarity.pl} 
that allows a user to test and demo the measures using a variety of 
command line options. In addition, there is a web interface that is based 
on {\it similarity.pl} that will be used in this demo. 

The package supports measures of similarity and measures of relatedness.  
A measure of similarity is based on the organization of concepts 
into an is--a hierarchy, which in WordNet limits such measures  
to nouns and verbs. These measures quantify how much concept A is like   
(or is similar to) concept B. In WordNet the noun and verb hierarchies are  
not connected,  so similarity measurements must be between two nouns 
(e.g., {\em cat} and {\em dog}) or a pair of verbs (e.g., {\em run} and 
{\em walk}). They may not cross part of speech boundaries. 

The measures of similarity provided include res \cite{Resnik95b}, lin  
\cite{Lin98}, jcn \cite{JiangC97}, lch  \cite{LeacockC98}, wup   
\cite{WuP94}, and a shortest path measure (edge) that usually serves as a  
baseline. The first three measures (res, lin, jcn) are based on the {\em 
information content} of the least common subsumer of the two concepts, and 
the information content of the concepts   themselves (in the case of lin 
and jcn). The  package also includes utility programs  that compute   
information content values from  corpora, including the SemCor, the Brown  
Corpus, the Penn Treebank, the  British National Corpus, and any corpus  
of plain text. The other  three measures (lch, wup, edge) are based in  
various ways on finding shortest paths between concepts. 

WordNet::Similarity also includes measures of relatedness, which take into  
account more information from WordNet than simply what is available in the  
is--a hierarchies.  As such they are able to measure the relatedness  
between concepts in different parts  of speech (e.g., the adjective  
{\em violent} and the noun {\em cabbage}). These measures can make   
comparisons  between any two concepts known to WordNet, since they do not   
require that the concepts reside in the same taxonomy and   consider  
relations in addition to is--a. These measures include hso  
\cite{HirstS98}, lesk  \cite{BanerjeeP03B}, and vector  
\cite{Patwardhan03}. 

The first (hso) finds  paths between concepts using nearly all of the  
relations available in WordNet, and allows those paths to cross 
from one part of speech to another. The last  two (lesk, vector) 
incorporate information from WordNet glosses in conjunction with WordNet  
structure and corpus statistics.  Finally, there is also a measure 
(random) that simply generates a random relatedness value for a pair of  
concepts.  This is useful to provide a lower bound on  performance when  
using relatedness measures in an application. 

\section{Using WordNet::Similarity}

WordNet::Similarity is implemented using Perl's object oriented features. 
It requires that the WordNet::QueryData package \cite{Rennie00} be   
installed, and that this be used to create an object representing WordNet. 
There are a number of methods available in WordNet::Similarity to a  
programmer,  including those that allow for the use of the existing  
measures, and also the creation of new measures. 

When an existing measure is to be used in a program, an object of that  
measure must be created via the new() method. Then the getRelatedness() 
method can be called for a pair of word senses, and this will return the 
relatedness value. For example, the following program will create an 
object of the Leacock and  Chodorow measure, and then find the similarity 
between the  first sense of the noun {\em car} (automobile) and the first  
and second sense of the noun {\em bus} (motorcoach and network bus). 

\begin{verbatim}
use WordNet::QueryData;
use WordNet::Similarity::lch;

$W=new WordNet::QueryData;

$L=new WordNet::Similarity::lch($W);

print $L->getRelatedness 
              ('car#n#1','bus#n#1');
print $L->getRelatedness 
              ('car#n#1','bus#n#2');

\end{verbatim}

The output from this program shows that the first pair of 
concepts (automobile and motorcoach) results in a similarity value of  
1.39, while the  second pair (automobile and network bus) scores 0.58.  
Higher values suggest greater similiarity, thus these results correspond  
to  our intuitive  understanding of the similarity between these pairs of  
concepts. The same information can be obtained from the command line  
program or web interface. 

There are also methods available that allow a user to construct their own 
measure. These include finding the shortest path and the least 
common subsumer between two concepts, retrieving the information content 
of a particular concept, and counting the number of words in common 
between the glosses of two concepts. This basic functionality allows for 
the re--implementation of nearly all of the existing measures, plus 
the development of new measures. 

For both new and existing measures, WordNet::Similarity enables detailed  
tracing that shows a variety of diagnostic information specific to each of 
the different kinds of measures. For example, for the measures that rely 
on path lengths (edge,  wup, lch) the tracing shows all the paths found  
between the concepts.  Tracing for the information content measures (res,  
lin, jcn) includes both  the paths between concepts as well as the least  
common subsumer. Tracing  for the hso measure shows the actual paths found  
through WordNet, while  the tracing for lesk shows the gloss overlaps in  
WordNet found for the  two concepts and their nearby relatives. The vector  
tracing shows the word vectors that are used to create the gloss vector  
of a concept. 

\section{Availability}

WordNet::Similarity is written in Perl and is freely distributed under the  
Gnu Public License. It is available from the Comprehensive Perl Archive 
Network ({\em search.cpan.org/dist/WordNet-Similarity}) and via  
SourceForge, an Open Source development platform ({\em 
wn-similarity.sourceforge.net}).   

\bibliographystyle{acl}
\bibliography{/home/cs/tpederse/TeX/Papers/papers/bib/tdp}

\end{document}

