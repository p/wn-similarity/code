%                             \\\|///                                %
%                             -(o o)-                                %
% -----------------------oOOOo--(_)--oOOOo-------------------------- %
%                                                                    %
%    Using Co--occurrence Vectors to Measure Semantic Relatedness    %
%    Using Gloss Vectors to Measure Semantic Relatedness?            %
%                                                                    %
%  Submitted to: AAAI '05                                            %
%                                                                    %
%  Authors: Siddharth Patwardhan <sidd@cs.utah.edu>                  %
%           Ted Pedersen <tpederse@d.umn.edu>                        %
%                                                                    %
%  $Id$         %
%                                                                    %
% ------------------------------------------------------------------ %

\documentclass[letterpaper]{article}

\usepackage{aaai}
\usepackage{times}
\usepackage{helvet}
\usepackage{courier}
\usepackage{latexsym}
\usepackage{graphicx}
\usepackage{epsfig}

\DeclareGraphicsExtensions{.jpg, .pdf, .mds, .png}

\title{Using Gloss Vectors to Measure Semantic Relatedness}

% submission is anonymous to aaai

\author{AUTHOR\\PLACE\\EMAIL}

%\author{Siddharth Patwardhan \\
%  School of Computing \\
%  University of Utah \\
%  Salt Lake City, UT 84112 \\
%  {\tt sidd@cs.utah.edu} \And
%  Ted Pedersen \\
%  Department of Computer Science \\
%  University of Minnesota Duluth \\
%  Duluth, MN 55812 \\  
%  {\tt tpederse@umn.edu}}

\date{}

\begin{document}

\maketitle

\begin{abstract}
This paper introduces the {\em Gloss Vector} measure of semantic    
relatedness, which combines dictionary content with co--occurrence   
information derived from raw corpora. The definitions or {\em glosses} 
associated with concepts in a dictionary are represented as second 
order context vectors, where each content word in a gloss is 
replaced by a first order context vector. The resultant of the first order   
vectors is used to represent the concept defined by the gloss. We assign 
numeric scores of relatedness to any pair of concepts by measuring  
the cosine between these second order representations of their 
glosses. This measure is flexible in that it can make comparisons 
between any two concepts without regard to their part of speech. In 
addition, it is adaptable since any corpora can be used to derive the 
word vectors. We show that this measure compares favorably to other 
measures with respect to human judgments of semantic relatedness, and that  
it performs well when used in a word sense disambiguation algorithm that 
relies on semantic relatedness.
\end{abstract}
  
\section{Introduction}

Humans are able to quickly judge the relative semantic relatedness of
concept pairs. For example, most would agree that {\em feather} is more
related to {\em bird} than it is to {\em tree}.  \cite{MillerC91} attribute
this human perception of relatedness to the overlap of contextual
representations of words in the human mind. These are presumably built up
throughout the life of an individual as she comes across concepts in text,
speech and across other representations of the concepts. 

Note that semantic relatedness and semantic similarity are not the same
idea. In fact, semantic similarity is a particular way that two concepts
can be related. It refers to the similarity of two concepts in their form
or usage, and would require such information about the concepts. For
example, {\em car} and {\em bus} are similar, because of their form (four
wheels and a body) and their usage (transport). However, {\em car} and {\em
driver}, though not similar, are highly related. In this paper we focus on
this more general notion of semantic relatedness. We are simply interested
in determining to what extent two concepts are related, without regards to
the nature of that relationship.

The ability to assess the semantic relatedness among concepts is an  
essential part of Natural Language Understanding. Consider the following  
example: {\em He swung the bat, hitting the ball into the stands}. 
A reader likely uses domain knowledge of sports along with the realization  
that the baseball senses of {\em hitting}, {\em bat}, {\em ball} and {\em  
stands} are all related, in order to determine that the event being  
described is a baseball game. 

In this paper we introduce the {\em Gloss Vector} measure of semantic
relatedness. This is based on using second order co-occurrence vectors
\cite{Schutze98} in combination with the structure and content of WordNet
\cite{Fellbaum98}, a semantic network of concepts. This measure captures
semantic information for concepts from contextual information drawn from
corpora of text.

WordNet consists of a network of {\em synsets}, each representing a real
world concept. Associated with each concept, is a definition or a gloss
describing that concept. A Gloss Vector is constructed for each concept
(synset) by using its definition (gloss) along with co--occurrence
information derived from a large corpus of text. We measure the semantic
relatedness between two concepts by taking the cosine of the angle between
their respective Gloss Vectors.

This paper is organized as follows. We start with a description of second   
order context vectors in general, and then define the Gloss Vector  
measure in particular. We present an extensive evaluation of the measure,  
both with respect to human relatedness judgments and also relative to  
its performance when used in a word sense disambiguation algorithm based  
on semantic relatedness. The paper concludes with  an analysis of our  
results, and some discussion of related and future work.

\section{Second Order Context Vectors}

Context vectors are widely used in Information Retrieval and Natural     
Language Processing. Most often they represent first order 
co--occurrences, which are simply words that occur near each other in a  
corpus of text. For example, {\em police} and {\em  car} are likely first  
order co--occurrences since they commonly occur together. A first order 
context vector for a given word would simply indicate all the first order 
co--occurrences of that word as found in a corpus. 

However, our Gloss Vector measure is based on second order    
co--occurrences. For example, if {\em car} and {\em mechanic} are first  
order co--occurrences, then {\em mechanic} and {\em police} would be  
second order co--occurrences since they are both  both first order  
co--occurrences of {\em car}. We follow \cite{Schutze98} and create  
a second order co--occurrence vector that represents an entire context 
(rather than just an individual word) by taking all the first order  
context vectors associated with the words that make up the context and  
adding them together, to create one resultant vector that represents  
the context. 

Sch\"utze's method starts by creating a {\em Word Space}, which is a
co--occurrence matrix where each row can be viewed as a first order context
vector. Each cell in this matrix represents the frequency with which two
words occur near one another in a corpus of text. The Word Space is usually
quite large and sparse, since while there are many words in the corpus,
most of them don't occur near each other. In order to reduce the
dimensionality and the amount of noise, non--content stop words such as
{\em the}, {\em for}, {\em a}, etc. are excluded from being rows or columns
in the Word Space.

Given a Word Space, a context can then be represented by second order
co--occurrences (context vector). This is done by finding the resultant of
the first order context vectors corresponding to each of the words in that
context. If a word in a context does not have a first order context vector
created for it, or if it is a stop word, then it is excluded from the
resultant.

For example, suppose we have the following context:

\begin{quote} 
  {\em The paintings were displayed in the art gallery.} 
\end{quote}

The second order context vector would be the average of the first order 
context vectors for for {\em painting}, {\em display}, {\em art}, and  
{\em gallery}. The words {\em were}, {\em in}, and {\em the} are excluded 
from the resultant since we consider them as stop words in this example. 
Figure \ref{fig:context} shows how the second order context vector might  
be visualized in a 2-dimensional space. 

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=7.7cm]{figures/context.eps}
\end{center}
\caption{Creating a context vector from word vectors}
\label{fig:context}
\end{figure}

Intuitively, the orientation of each second order context vector 
is an indicator of the domains or topics (such as {\em biology} or {\em 
baseball}) that the context is associated with. Two context vectors that 
lie close together indicate a considerable contextual overlap, which 
suggests that they are pertaining to the same meaning of the target word.

\section{Gloss Vectors in Semantic Relatedness}

In this research, we create a Gloss Vector for each concept (or word sense)
represented in a dictionary. While we use WordNet as our dictionary, the
method can apply to other lexical resources. A Gloss Vector is a second
order context vector formed by treating the dictionary definition of a
concept as a context, and finding the sum of the first order context
vectors of the words in the definition.

In particular, we define a Word Space by creating first order context   
vectors for every word $w$ that is not a stop word and that occurs above a 
minimum frequency in our corpus. The specific steps are as follows:
\begin{enumerate}
\item Initialize the first order context vector to a zero vector 
$\stackrel{\rightarrow}{w}$.
\item Find every occurrence of $w$ in the given corpus.
\item For each occurrence of $w$, increment those dimensions of
  $\stackrel{\rightarrow}{w}$ that correspond to the words from the Word
  Space and are present within a given number of positions around $w$ in  
  the corpus.
\end{enumerate}
The first order context vector $\stackrel{\rightarrow}{w}$, therefore,  
encodes the co--occurrence information of word $w$. 

For example, consider the gloss of {\em lamp} -- {\em an artificial source
of visible illumination}. The Gloss Vector for {\em lamp} would be formed
by averaging the first order context vectors of {\em artificial},
{\em source}, {\em visible} and {\em illumination}.

The corpus of text we use in these experiments for deriving first order
context vectors is WordNet. We take the glosses for all of the
concepts in WordNet and view that as a large corpus of text. This corpus
consists of approximately 1.4 million words, and results in a square Word
Space of approximately 20,000 dimensions, once low frequency and stop words
are removed. We chose the WordNet glosses as a corpus because we felt the
glosses were likely to contain content rich terms that would distinguish
between the various concepts more distinctly than would text drawn from a
more generic corpus. However, in our future work we will experiment with
other corpora as the source of first order context vectors, and other
dictionaries as the source of glosses.

This formulation of the Gloss Vector measure is independent of the
dictionary used and is independent of the corpus used, and hence is quite
flexible. However, dictionary glosses tend to be rather short, and it is 
possible that even closely related concepts will be defined using  
different sets of words. Our belief is that two synonyms that are used in  
different glosses will tend to have similar Word Vectors (because their 
co--occurrence behavior should be similar). However, the brevity of 
dictionary glosses may still make it difficult to measure relatedness 
despite this characteristic. 

\cite{BanerjeeP03} experience a similar problem when measuring semantic    
relatedness by counting the number of matching words between the glosses 
of two different concepts. They expand the glosses of concepts in WordNet 
with the glosses of concepts that are directly linked by a WordNet  
relation.  We adopt the same technique here, and use the relations in    
WordNet to augment glosses for the Gloss Vector  measure. We take the  
gloss of a concept $c$, and concatenate to it the glosses of all the 
concepts to which it is directly related according to WordNet. The Gloss 
Vector is then created from this big concatenated gloss.

\begin{figure}
\begin{center}
\includegraphics[width=7.7cm]{figures/vectors.eps}
\end{center}
\caption{{\em First Order Context Vectors} and a {\em Gloss Vector}}
\label{fig:vectors}
\end{figure}

The first order context vectors as well as the Gloss Vectors usually have  
a very large number of dimensions (usually tens of thousands) and it is  
very difficult to visualize this space. Figure \ref{fig:vectors} attempts  
to illustrate these vectors in two dimensions.  {\em Tennis} and {\em 
food} are the dimensions of this 2-dimensional space. We see that the 
first order context vector for {\em serve} is approximately halfway  
between {\em tennis} and {\em food}, since the word {\em serve} could mean  
to ``serve the ball'' in the context of tennis or could mean ``to serve  
food'' in another context. 

The first order context vectors for {\em eat} and {\em  cutlery} are very  
close to {\em food}, since they do not have a sense that is related to  
tennis. The gloss for the word {\em fork}, ``cutlery used to serve and eat  
food'', contains the words {\em cutlery}, {\em serve}, {\em eat} and {\em  
food}. The Gloss Vector for {\em fork} is formed by adding the first 
order context vectors of {\em cutlery}, {\em serve}, {\em eat} and {\em  
food}. Thus,  {\em fork} has a Gloss Vector which is heavily weighted  
towards {\em food}. {\em Food} is, therefore, in the same domain as and  
related to the concept of {\em fork}. 

Similarly, we expect that in a high dimensional space, the Gloss Vector of
{\em fork} would be heavily weighted towards all concepts in the same
domain of the concept of {\em fork}. Additionally, the previous
demonstration involved a small gloss for representing {\em fork}. Using
augmented glosses, we achieve better representations of concepts to build
Gloss Vectors upon.

\section{Other Measures of Relatedness}

Below we briefly describe five alternative measures of semantic relatedness,
and then go on to include them as points of comparison in our experimental
evaluation of the Gloss Vector measure. All of these measures depend in
some way upon WordNet. Four of them limit their measurements to nouns
located in the WordNet {\em is-a} hierarchy.

Each of these measures takes two WordNet concepts (i.e., word senses or
synsets) $c_1$ and $c_2$ as input and return a numeric score that
quantifies their degree of relatedness.

\begin{itemize}

\item \cite{LeacockC98} finds the path length between $c_1$ and $c_2$ in
  the {\em is-a} hierarchy of WordNet. The path length is then scaled by
  the depth of the hierarchy (D) in which they reside to obtain the
  relatedness of the two concepts.

\item \cite{Resnik95B} introduced a measure that is based on {\em
  information content}, which is a value that indicates the specificity of
  a concept. These values are derived from corpora, and are used to augment
  the concepts in the WordNet {\em is-a} hierarchy. The measure of
  relatedness between two concepts is the information content of the most
  specific concept that both concepts have in common (i.e., their lowest
  common subsumer in the {\em is-a} hierarchy).
  
\item \cite{JiangC97} extends Resnik's measure to combine the information
  contents of $c_1$, $c_2$ and their lowest common subsumer.
  
\item \cite{Lin98} also extends Resnik's measure, by taking the ratio of
  the shared information content to that of the individual concepts.

\item \cite{BanerjeeP03} introduce Extended Gloss Overlaps, which is a
  measure that determines the relatedness of concepts proportional to the
  extent of overlap of their WordNet glosses. This simple definition is
  extended to take advantage of the complex network of relations in
  WordNet, and allows the glosses of concepts to include the glosses of
  synsets to which they are directly related in WordNet.
\end{itemize}

\section{Evaluation}

As was done by \cite{BudanitskyH01}, we evaluated the measures of
relatedness in two ways. First, they were compared against human judgments of
relatedness. Second, they were used in an application that would benefit
from the measures. The effectiveness of the particular application was an
indirect indicator of the accuracy of the relatedness measure
used. Clearly, the effect of the application itself needs to be factored
into the analysis of the latter set of results.

\subsection{Comparison with Human Judgment}

One obvious metric for evaluating a measure of semantic relatedness is its
correspondence with the human perception of relatedness. Since semantic
relatedness is subjective and depends on the human view of the world, it
only makes sense to compare the measures with human judgement. This was
done by \cite{BudanitskyH01} in their comparison of five measures of
semantic relatedness. We follow a similar approach in evaluating
the Gloss Vector measure.

We use a set of 30 word pairs from a study carried out by
\cite{MillerC91}. These word pairs are a subset of 65 word pairs used by
\cite{RubensteinG65}, in a similar study almost 25 years earlier. In this
study, human subjects assigned relatedness scores to the selected word
pairs. The word pairs selected for this study ranged from highly related
pairs to unrelated pairs. We use these human judgements for our
evaluation.

Each of the word pairs have been scored by humans on a scale of 0 to 5,
where 5 is the most related. The mean of the scores of each pair from all
subjects is considered as the ``human relatedness score'' for that
pair. The pairs are then ranked with respect to their scores. The most
related pair is the first on the list and the least related pair is at the
end of the list. We then have each of the measures of relatedness score the
word pairs and a another ranking of the word pairs is created corresponding
to each of the measures.

Spearman's \cite{Spearman04} Correlation Coefficient is used to assess the
closeness of two rankings.  If the two rankings are exactly the same,
the Spearman's correlation coefficient between these two rankings is $1$. A
completely reversed ranking gets a value of $-1$. The value is 0 when there
is no relation between the rankings.

% Table of the correlation of the basic run of all the measures.
\begin{table}
  \caption{Correlation to human perception of relatedness}
  \label{tab:results1}
  \begin{center}
    \begin{tabular}{|l|c|c|}
      \hline
      \multicolumn{1}{|c|}{Relatedness Measures} &
      \multicolumn{1}{|c|}{M \& C} &
      \multicolumn{1}{|c|}{R \& G} \\
      \hline
      Gloss Vector                      & 0.91 & 0.90 \\
      Extended Gloss Overlaps           & 0.81 & 0.83 \\
      Jiang \& Conrath                  & 0.73 & 0.75 \\ % (using default -- semcor)
      Resnik                            & 0.72 & 0.72 \\ % (using default -- semcor)
      Lin                               & 0.70 & 0.72 \\ % (using default -- semcor)
      Leacock \& Chodorow               & 0.74 & 0.77 \\
      \hline
    \end{tabular}
  \end{center}
\end{table}

We determine the correlation coefficient of the ranking of each measure
with that of the human relatedness. We use the relatedness scores from both
the human studies -- the Miller and Charles study as well as the Rubenstein
and Goodenough research. Table \ref{tab:results1} summarizes the results of
our experiment. We observe that the Gloss Vector has the highest
correlation with humans in both cases. 

Note that in our experiments with the Gloss Vector measure, we have used
not only the gloss of the concept but augmented that with the gloss of all
the concepts directly related to it according to WordNet. We observed a
significant drop in performance when we used just the glosses of the
concept alone, showing that the expansion is necessary. In addition, the
frequency cutoffs used to construct the Word Space played a critical role. The
best setting of the frequency cutoffs removed both low and high frequency
words, which eliminates two different sources of noise. Very low frequency
words to not occur enough to draw distinctions among different glosses,
whereas high frequency words occur in many glosses, and again do not
provide useful information to distinguish among glosses.

\subsection{Application-based Evaluation}

An application-oriented comparison of five measures of semantic relatedness
was presented in \cite{BudanitskyH01}. In that study they evaluate five
WordNet-based measures of semantic relatedness with respect to their
performance in context sensitive spelling correction.

We present the results of an application-oriented evaluation of the
measures of semantic relatedness. Each of the seven measures of semantic
relatedness was used in a word sense disambiguation algorithm described by
Banerjee and Pedersen.

Word sense disambiguation is the task of determining the meaning (from
multiple possibilities) of a word in a given context. For example, in the
sentence {\em The ex-cons broke into the bank on Elm street}, the word {\em
bank} has the ``financial institution'' sense as opposed to the ``edge of a
river'' sense.

Banerjee and Pedersen attempt to perform this task measuring the
relatedness of the senses of the target word to those of the words in its
context. The sense of the target word that is most related to its context
is selected as the implied sense of the target word.

The experimental data used for this evaluation is the \textsc{Senseval-2}
test data. This consists of 4,328 instances (or contexts) that include a
single ambiguous target word. Each instance consists of approximately 2-3
sentences and one occurrence of a target word. 1,754 of the instances
include nouns as target words, while 1,806 are verbs and 768 are
adjectives. We use the noun data to compare all six of the measures, since
four of the measures are limited to nouns as input.  The accuracy of
disambiguation when performed using each of the measures for nouns is shown
in Table \ref{tab:results2}.

\begin{table}
  \caption{WSD on \textsc{Senseval-2} (nouns)}
  \label{tab:results2}
  \begin{center}
    \begin{tabular}{|l|c|}
      \hline
      \multicolumn{1}{|c|}{Measure} & 
      \multicolumn{1}{|c|}{Nouns} \\
      \hline
      Jiang \& Conrath        & 0.45 \\
      Extended Gloss Overlaps & 0.44 \\
      Gloss Vector            & 0.41 \\
      Lin                     & 0.36 \\
      Resnik                  & 0.30 \\
      Leacock \& Chodorow     & 0.30 \\
      \hline
    \end{tabular}
  \end{center}
\end{table}

\section{Gloss Vector Tuning}

As discussed in earlier sections, the Gloss Vector measure builds a word
space consisting of first order context vectors corresponding to every word
in a corpus. Gloss vectors are the resultant of a number of first order
context vectors. All of these vectors encode semantic information about the
word or the gloss that the vectors represent.

We note that the quality of the words used as the dimensions of these
vectors plays a big part in getting accurate relatedness scores. We find
that words corresponding to very specific concepts and are highly indicative
of a few topics, make good dimensions. Words that are very general in
nature and that appear all over the place add noise to the vectors.

In an earlier section we discussed using stop words and frequency cutoffs
to eliminate the general words and keep only the high ``information
content'' words. In addition to those, we also experimented with a
$term frequency \cdot inverse document frequency$ cutoff.

{\em Term frequency} and {\em inverse document frequency} are commonly used
metrics in information retrieval. For a given word, term frequency is the
number of times a word appears in the corpus, while document frequency is
number of documents in which the word occurs. Using these definitions we
computed the $tf \cdot idf$ value for every word in the corpus as
\begin{equation}
  tf\cdot idf = Term Frequency \times {\rm log}\frac{Number of
    Documents}{Document Frequency}
\end{equation}

The $tf\cdot idf$ value is an indicator of the specificity of a
word. The higher the $tf \cdot idf$ value, lower the specificity. 

Figure \ref{fig:graph} shows a plot of $tf\cdot idf$ cutoff on the x-axis
against the correlation of the Gloss Vector measure with human judgements
on the y-axis.

\begin{figure}[h]
\begin{center}
\includegraphics[width=7.7cm]{figures/graph.eps}
\end{center}
\caption{Plot of $tf\cdot idf$ cutoff vs. correlation}
\label{fig:graph}
\end{figure}

The $tf\cdot idf$ values ranged from 0 to about 4200.  As can be seen
from the graph the best accuracy was obtained with a $tf\cdot idf$ cutoff
of about 1500.

\section{Analysis}

We observe from the experimental results that the Gloss Vector measure
corresponds the most with human judgement of relatedness (with a
correlation of almost 0.9). We believe this high correlation possibly
because, in our minds, we (humans) measure relatedness as contextual
overlaps of concepts. In our mind, a contextual representation of a concept
comprises of different formats -- such as auditory, visual, etc. In the
Gloss Vector measure the contextual representation is captured in a context
vector, and of the measures is most similar to this notion of the human
mind.

In the application-oriented evaluation also, the Gloss Vector measure
performs relatively well (about 41\% accuracy). Despite its excellent
correlation with human relatedness, why did the Gloss Vector measure not
outperform all the other measure in the application-oriented evaluation? To
explain this anomaly, one should note that the application itself affects
the performance of the measure. The Word Sense Disambiguation algorithm
starts by selecting a context of 5 words from around the target word. These
context words contain words from all parts of speech. Since the
Jiang-Conrath measure abstains from assigning relatedness scores to
non-nouns concepts, its behaviour would differ from that of the Vector
measure which would accept all words and would be affected by the noise
introduced from unrelated concepts. Thus the context selection factors into
the accuracy obtained. However, for evaluating the measure as being
suitable for use in real applications, the Gloss Vector measure proves
highly accurate.

The Gloss Vector measure is a true measure of relatedness, and can draw
conclusions about any two concepts, irrespective of part-of-speech. The
only other measure that can make this same claim is the Extended Gloss
Overlaps measure. We would argue that Gloss Vectors present certain
advantages over it.

The Extended Gloss Overlap measure looks for exact string overlaps to
measure relatedness. This ``exactness'' works against the measure, in that
it misses potential matches that intuitively would contribute to the score
(For example, {\em silverware} with {\em spoon}). The Gloss Vector measure
is more robust than the Extended Gloss Overlap measure, in that exact
matches are not required to identify relatedness. The Gloss Vector measure
attempts to overcome this ``exactness'' by using vectors that capture the
contextual representation of all words. So even though {\em silverware} and
{\em spoon} do not overlap, their contextual representations would overlap
to some extent.

\section{Related Work}

\cite{WilksFGMPS93} describe a word sense disambiguation algorithm that
uses context vectors to determine the correct sense of a word. In their
approach, they use dictionary definitions from LDOCE. The words in these
definitions are used to build a co-occurrence matrix, which is very similar
to our technique of using the WordNet glosses as our source of information
for our Word Space. Each word can then be represented by a vector where
each dimension shows often it occurs with another of the other
words. However, at this point our techniques diverge, in that they use the
co--occurrence matrix to identify words that are similar to the words that
appear in definitions, which are then expanded to include those similar
words from the co--occurrence matrix. The vectors corresponding to every
word in the expanded definitions are averaged into a single vector that
represents that particular sense.

Their word sense disambiguation algorithm continues by expanding the
context in which a target word occurs to include words that are related to
those already in the context. Vectors corresponding to all the words in the
expanded context are averaged to form a vector that represents the context,
and this is the compared with the vectors of the possible senses of the
target word. The sense associated with the vector that is most similar to
the context of the target word is selected.

This is somewhat similar to our own Gloss Vector measure, although we do
not expand the glosses with similar words and we use the synset relations
in WordNet increase the quantity of information present in the vectors.

\cite{NiwaN94} compare dictionary based vectors with co-occurrence based
vectors, where the vector of a word is the probability that an origin word
occurs in the context (in a given window of words) of the word. These two
representations are evaluated by applying them to real world applications
and quantifying the results. Both measures are first applied to {\em word
sense disambiguation} and then to the {\em learning of positives or
negatives}, where it is required to determine whether a word has a positive
or negative meaning. It was observed that the co-occurrence based idea
works better for the {\em word sense disambiguation} and the dictionary
based approach gives better results for the {\em learning of positives or
negatives}. From this, the conclusion is that the dictionary based vectors
contain some different semantic information about the words and warrants
further investigation. It is also observed that for the dictionary based
vectors, the network of words is almost independent of the dictionary that
is used, i.e. any dictionary gives us almost the same
network. Consequently, we are presented with yet another novel way to think
about the semantic relatedness of words.

\cite{InkpenH03} also use gloss--based context vectors in their work on
the disambiguation of near--synonyms. These are words whose senses are
almost indistinguishable, with only a fine difference between their
senses. They disambiguate near-synonyms in text using various {\em
indicators}, one of which is context-vector-based similar to that
described by \cite{Schutze98}.  Context Vectors are created for the context
of the target word and also for the glosses of each sense of the target
word. Each gloss is considered as a bag of words, where each word has a
corresponding Word Vector. These vectors for the words in a gloss are
averaged to get a Context Vector corresponding to the gloss. The distance
between the vector corresponding to the text and that corresponding to the
gloss was measured (as the cosine of the angle between the vectors). The
nearness of the vectors is used as an indicator to pick the correct sense
of the target word.

\section{Conclusion}

We introduced a new measure of semantic relatedness based on the idea of
creating a Gloss Vector that combines dictionary content with corpus based
data. We find that this measure correlates extremely well with the results
of these human studies, and this is indeed encouraging. We believe that
this is due to the fact that the context vector is making its relatedness
judgments based on the actual context in which words occur, while the other
measures rely more on the existing structure of relations within WordNet to
draw their conclusions. This measure can be tailored to particular domains
depending on the corpus used to derive the co-occurrence matrices, and
makes no restrictions on the parts of speech of the concept pairs to be
compared. This is not true of most of the other measures. The Resnik, Jiang
and Conrath, Lin, Leacock and Chodorow measures are limited to studying
noun-noun concept pairs, which is somewhat limiting when thinking of
applying such measures to applications.

We also demonstrated that the Vector measure is sufficiently fast and
accurate in a Word Sense Disambiguation algorithm. It performs relatively
well in an application-oriented setup and can be easily deployed in a real
world application. It can be easily tweaked and modified to work in a
restricted domain, such as bioinformatics or medicine, by selecting a
specialized corpus to build the vectors and by changing parameters such as
frequency cutoffs and the $tf\cdot idf$ cutoff.

%%%\section*{Acknowledgements}
%%% don't need in anonymous submission

\bibliographystyle{aaai}
\bibliography{References}

\end{document}
